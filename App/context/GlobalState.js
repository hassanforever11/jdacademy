import React, { useMemo, useReducer } from 'react'

import * as ActionCreators from './ActionCreators'
import { AuthContext } from './Contexts'
import { loginReducer } from './reducer'

const initialState = {
  isLoading: false,
  username: null,
  name: null,
  userToken: null,
  uid: null,
  profile_image: null,
  pid: null,
  mail: null,
  exam_category: [],
  mobile_number: ''
}

export default function GlobalState(props) {
  const [loginState, dispatch] = useReducer(loginReducer, initialState)

  const actions = useMemo(() => ({
    signIn: (user, pass) => ActionCreators.login(dispatch, { user, pass }),
    signUp: (data) => ActionCreators.register(dispatch, data),
    signOut: () => ActionCreators.logout(dispatch),
    retrieveToken: () => ActionCreators.retrieveToken(dispatch),
    sendOtp: (data) => ActionCreators.sendOtp(dispatch, data),
    verifyOtp: (data) => ActionCreators.verifyOtp(dispatch, data),
    getProfile: () => ActionCreators.getProfile(dispatch),
    updateProfile: (data) => ActionCreators.updateProfile(dispatch, data),
    forgetPassword: (data) => ActionCreators.forgetPassword(dispatch, data)
  }))

  return (
    <AuthContext.Provider value={{ ...loginState, actions }}>
      {props.children}
    </AuthContext.Provider>
  )
}
