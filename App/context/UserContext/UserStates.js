/* eslint-disable no-unused-vars */
import React, { useMemo, useReducer } from 'react'

import * as ActionCreators from './ActionCreators'
import { UserContext } from '../Contexts'
import reducer from './reducer'

const initialState = {
  userLoading: false,
  category: [],
  sessionList: null,
  videoList: null,
  notes: null,
  tabData: {},
  library: null,
  quiz: null,
  quizData: null
}

export default function GlobalState(props) {
  const [userState, dispatch] = useReducer(reducer, initialState)

  const userActions = useMemo(() => ({
    getAllCategory: () => ActionCreators.getAllCategory(dispatch),
    fetchAboutUs: () => ActionCreators.fetchAboutUs(dispatch),
    addCourse: (data, authActions) =>
      ActionCreators.addCourse(dispatch, data, authActions),
    getCourseList: (tid) => ActionCreators.getCourseList(dispatch, tid),
    getSessionList: (cid) => ActionCreators.getSessionList(dispatch, cid),
    getNotes: (sid) => ActionCreators.getNotes(dispatch, sid),
    getVideos: (sid) => ActionCreators.getVideos(dispatch, sid),
    getQuiz: (sid) => ActionCreators.getQuiz(dispatch, sid),
    getQuizData: (qid) => ActionCreators.getQuizData(dispatch, qid),
    getMyLibrary: (uid) => ActionCreators.getMyLibrary(dispatch, uid),
    getMyLibrary: (uid) => ActionCreators.getMyLibrary(dispatch, uid)
  }))

  return (
    <UserContext.Provider value={{ ...userState, userActions }}>
      {props.children}
    </UserContext.Provider>
  )
}
