/* eslint-disable no-unused-vars */
import Axios from '../../utility/axiosInstance'
import { Alert } from 'react-native'
import AsyncStorage from '@react-native-community/async-storage'
import base64 from 'react-native-base64'

import * as Types from './Types'
import { getProfile } from '../ActionCreators'
import { showToast } from '../../utility/commonMethods'

export const getAllCategory = (dispatch, req) => {
  dispatch({ type: Types.REQUEST })
  Axios.get('/get_allcategory')
    .then((res) => {
      if (res.data && res.data.length) {
        dispatch({ type: Types.GET_CATEGORY, category: res.data })
      }
    })
    .catch((err) => {
      //  eslint-disable-next-line no-console
      console.log('[getAllCategory HANDLE]: ', err)
    })
    .finally(() => {
      dispatch({ type: Types.LOADER_OFF })
    })
}

export const addCourse = (dispatch, req, authActions) => {
  dispatch({ type: Types.REQUEST })
  const { uid } = req
  Axios.put(`/user/${uid}`, req)
    .then((res) => {
      if (res.status === 200) {
        showToast(
          req.is_remove === '1' ? 'Removed Successfully' : 'Added Successfully',
          'success',
          1500
        )
        if (authActions) authActions.getProfile(dispatch)
      } else {
        showToast('Something went wrong!', 'danger')
      }
    })
    .catch((err) => {
      //  eslint-disable-next-line no-console
      console.log('[addCourse HANDLE]: ', err)
      showToast('Something went wrong!', 'danger')
    })
    .finally(() => {
      dispatch({ type: Types.LOADER_OFF })
    })
}

export const getCourseList = (dispatch, tid) => {
  dispatch({ type: Types.REQUEST })
  Axios.get(`/course_listing?course_category_tid=${tid}`)
    .then((res) => {
      if (res.status === 200 && res.data) {
        dispatch({
          type: Types.GET_COURSE_LISTING,
          courseList: res.data,
          tid
        })
      } else {
        dispatch({
          type: Types.GET_COURSE_LISTING,
          courseList: [],
          tid
        })
      }
    })
    .catch((err) => {
      //  eslint-disable-next-line no-console
      console.log('[getCourseList HANDLE]: ', err)
      showToast('Could not fetch data try again', 'danger')
      dispatch({ type: Types.GET_COURSE_LISTING, courseList: [] })
    })
    .finally(() => {
      dispatch({ type: Types.LOADER_OFF })
    })
}

export const getSessionList = (dispatch, courseId) => {
  dispatch({ type: Types.REQUEST })
  Axios.get(`/session_listing?cid=${courseId}`)
    .then((res) => {
      if (res.data) {
        dispatch({
          type: Types.GET_SESSION_LISTING,
          sessionList: res.data
        })
      } else {
        dispatch({ type: Types.GET_SESSION_LISTING, sessionList: [] })
      }
    })
    .catch((err) => {
      //  eslint-disable-next-line no-console
      console.log('[getSessionList HANDLE]: ', err)
      showToast('Could not fetch data try again', 'danger')
      dispatch({ type: Types.GET_SESSION_LISTING, sessionList: [] })
    })
    .finally(() => {
      dispatch({ type: Types.LOADER_OFF })
    })
}

export const getNotes = (dispatch, sessionId) => {
  dispatch({ type: Types.REQUEST })
  Axios.get(`/notes_listing?sid=${sessionId}`)
    .then((res) => {
      if (res.status === 200 && res.data) {
        dispatch({
          type: Types.GET_NOTES,
          notes: res.data
        })
      } else {
        dispatch({ type: Types.GET_NOTES, notes: [] })
      }
    })
    .catch((err) => {
      //  eslint-disable-next-line no-console
      console.log('[getvideoList HANDLE]: ', err)
      showToast('Could not fetch data try again', 'danger')
      dispatch({ type: Types.GET_NOTES, notes: [] })
    })
    .finally(() => {
      dispatch({ type: Types.LOADER_OFF })
    })
}

export const getVideos = (dispatch, sessionId) => {
  dispatch({ type: Types.REQUEST })
  Axios.get(`/video_listing?sid=${sessionId}`)
    .then((res) => {
      if (res.status === 200 && res.data) {
        dispatch({
          type: Types.GET_VIDEOS,
          videoList: res.data
        })
      } else {
        dispatch({ type: Types.GET_VIDEOS, videoList: [] })
      }
    })
    .catch((err) => {
      //  eslint-disable-next-line no-console
      console.log('[getvideoList HANDLE]: ', err)
      showToast('Could not fetch data try again', 'danger')
      dispatch({ type: Types.GET_VIDEOS, videoList: [] })
    })
    .finally(() => {
      dispatch({ type: Types.LOADER_OFF })
    })
}

export const getQuiz = (dispatch, sid) => {
  dispatch({ type: Types.REQUEST })
  Axios.get(`/quiz_listing?sid=${sid}`)
    .then((res) => {
      if (res.status === 200 && res.data) {
        dispatch({
          type: Types.GET_QUIZ,
          quiz: res.data
        })
      } else {
        dispatch({ type: Types.GET_QUIZ, quiz: [] })
      }
    })
    .catch((err) => {
      //  eslint-disable-next-line no-console
      console.log('[getquiz HANDLE]: ', err)
      showToast('Could not fetch data try again', 'danger')
      dispatch({ type: Types.GET_QUIZ, quiz: [] })
    })
    .finally(() => {
      dispatch({ type: Types.LOADER_OFF })
    })
}

export const getQuizData = (dispatch, qid) => {
  dispatch({ type: Types.REQUEST_QUIZ_DATA })
  Axios.get(`/quiz_questions/${qid}`)
    .then((res) => {
      if (res.status === 200 && res.data) {
        dispatch({
          type: Types.GET_QUIZ_DATA,
          quizData: res.data
        })
      } else {
        dispatch({ type: Types.GET_QUIZ_DATA, quizData: [] })
      }
    })
    .catch((err) => {
      //  eslint-disable-next-line no-console
      console.log('[getquizData HANDLE]: ', err)
      showToast('Could not fetch data try again', 'danger')
      dispatch({ type: Types.GET_QUIZ_DATA, quizData: [] })
    })
    .finally(() => {
      dispatch({ type: Types.LOADER_OFF })
    })
}

export const getMyLibrary = (dispatch, uid) => {
  dispatch({ type: Types.REQUEST })
  Axios.get(`/user_library?uid=${uid}`)
    .then((res) => {
      if (res.status === 200 && res.data) {
        dispatch({
          type: Types.ADD_TO_LIBRARY,
          library: res.data
        })
      } else {
        dispatch({
          type: Types.ADD_TO_LIBRARY,
          library: []
        })
      }
    })
    .catch((err) => {
      //  eslint-disable-next-line no-console
      console.log('[getCourseList HANDLE]: ', err)
      showToast('Could not fetch data try again', 'danger')
      dispatch({ type: Types.ADD_TO_LIBRARY, library: [] })
    })
    .finally(() => {
      dispatch({ type: Types.LOADER_OFF })
    })
}

export const fetchAboutUs = (dispatch) => {
  dispatch({ type: Types.REQUEST })
  Axios.get(`/about_us`)
    .then((res) => {
      if (res.status === 200 && res.data) {
        const [{ description, title }] = res.data
        dispatch({
          type: Types.ABOUT_US,
          aboutUs: { description, title }
        })
      } else {
        dispatch({
          type: Types.ABOUT_US,
          aboutUs: []
        })
      }
    })
    .catch((err) => {
      //  eslint-disable-next-line no-console
      console.log('[getCourseList HANDLE]: ', err)
      showToast('Could not fetch data try again', 'danger')
      dispatch({ type: Types.ABOUT_US, aboutUs: [] })
    })
    .finally(() => {
      dispatch({ type: Types.LOADER_OFF })
    })
}
