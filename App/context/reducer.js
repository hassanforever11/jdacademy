import * as Types from './Types'

const initialState = {
  isLoading: false,
  username: null,
  name: null,
  userToken: null,
  uid: null,
  profile_image: null,
  pid: null,
  mail: null,
  exam_category: [],
  mobile_number: ''
}

export const loginReducer = function (state, action) {
  switch (action.type) {
    case Types.REQUEST:
      return { ...state, isLoading: true }
    case Types.LOADER_OFF:
      return { ...state, isLoading: false }
    case Types.RETRIEVE_TOKEN:
      return { ...state, isLoading: false, userToken: action.userToken }
    case Types.LOGIN:
      return {
        ...state,
        isLoading: false,
        uid: action.uid,
        mail: action.mail,
        username: action.name,
        userToken: action.token
      }
    case Types.LOGOUT:
      return initialState
    case Types.GET_PROFILE:
      return {
        ...state,
        name: action.name,
        uid: action.uid,
        profile_image: action.profile_image,
        pid: action.pid,
        exam_category: action.exam_category,
        mobile_number: action.mobile_number
      }

    default:
      return state
  }
}
