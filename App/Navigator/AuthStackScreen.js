import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'

import SignIn from '../containers/Authentication/SignIn'
import ForgetPass from '../containers/Authentication/ForgetPass'
import SignUp from '../containers/Authentication/SignUp'
import SignUpOTP from '../containers/Authentication/OTP'
import Landing from '../containers/Authentication/Landing'

import * as routes from '../utility/routes'

const AuthStack = createStackNavigator()

const AuthStackScreen = () => {
  return (
    <AuthStack.Navigator screenOptions={{ headerShown: false }}>
      <AuthStack.Screen name={routes.Landing} component={Landing} />
      <AuthStack.Screen name={routes.SignIn} component={SignIn} />
      <AuthStack.Screen name={routes.SignUp} component={SignUp} />
      <AuthStack.Screen name={routes.SignUpOTP} component={SignUpOTP} />
      <AuthStack.Screen name={routes.ForgetPassword} component={ForgetPass} />
    </AuthStack.Navigator>
  )
}

export default AuthStackScreen
