import axios from 'axios'
import AsyncStorage from '@react-native-community/async-storage'

const axiosInstance = axios.create({
  baseURL: 'https://www.onlinelearning.aimzo.co/api'
})

const isHandlerEnabled = (config = {}) => {
  return config.hasOwnProperty('handlerEnabled') && !config.handlerEnabled
    ? false
    : true
}

const requestHandler = async (request) => {
  if (isHandlerEnabled(request)) {
    // Modify request here
    const token = await AsyncStorage.getItem('userToken')
    if (token) request.headers['Authorization'] = `Basic ${token}`
  }
  return request
}

const errorHandler = (error) => {
  if (isHandlerEnabled(error.config)) {
    // Handle errors
  }
  return Promise.reject({ ...error })
}

const successHandler = (response) => {
  if (isHandlerEnabled(response.config)) {
    // Handle responses
  }
  return response
}

axiosInstance.interceptors.request.use((request) => requestHandler(request))

axiosInstance.interceptors.response.use(
  (response) => successHandler(response),
  (error) => errorHandler(error)
)

export default axiosInstance
