import React, { useState, useContext, useRef, useEffect } from 'react'
import {
  Text,
  View,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  Platform,
  StatusBar
} from 'react-native'
import * as Animatable from 'react-native-animatable'
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import Feather from 'react-native-vector-icons/Feather'
import OTPTextInput from 'react-native-otp-textinput'
import RNOtpVerify from 'react-native-otp-verify'

import * as routes from '../../utility/routes'
import { AuthContext } from '../../context/Contexts'
import colors from '../../styles/colors'
import { Toast } from 'native-base'

const btnText = ['Request OTP', 'Reset Password']
const RESEND_OTP_TIME_LIMIT = 50
const INPUT_COUNT = 4
let resendOtpTimerInterval

function validateMobile(val) {
  const re = /^[6-9]\d{9}$/
  return re.test(val)
}

const ForgetPassword = ({ navigation }) => {
  const [data, setData] = useState({
    mobile: '',
    password: '',
    check_textInputChange: false,
    secureTextEntry: true,
    isValidMobile: true,
    isValidPassword: true
  })

  const [level, setLevel] = useState(0)
  const otpInput = useRef(null)
  const [otp, setOtp] = useState('')

  const { actions } = useContext(AuthContext)

  const [resendButtonDisabledTime, setResendButtonDisabledTime] = useState(
    RESEND_OTP_TIME_LIMIT
  )

  const setText = (otp) => {
    otpInput?.current?.setValue(otp)
  }

  // handle auto read otp once we received message and auto submit
  const handleAutoReadOtp = (message) => {
    try {
      if (message) {
        var rx = /(\d{4})/
        let extractedOtp = rx.exec(message)
        if (extractedOtp && extractedOtp.length) {
          setText(extractedOtp[0].toString())
          setOtp(extractedOtp[0].toString())
        }
      }
    } catch (error) {
      showToast('Unable to read message', 'danger')
    }
  }

  useEffect(() => {
    // docs: https://github.com/faizalshap/react-native-otp-verify
    RNOtpVerify.getOtp()
      .then((p) => RNOtpVerify.addListener(handleAutoReadOtp))
      .catch((error) => {
        showToast('Auto Read Otp Not working', 'danger')
      })

    // remove listener on unmount
    return () => {
      RNOtpVerify.removeListener()
    }
  }, [])

  const handleResendOtp = () => {
    setResendButtonDisabledTime(RESEND_OTP_TIME_LIMIT)
    actions.sendOtp({
      mobile: data.mobile
    })
  }
  useEffect(() => {
    if (level === 1) startResendOtpTimer()

    return () => {
      if (resendOtpTimerInterval) {
        clearInterval(resendOtpTimerInterval)
      }
    }
  }, [resendButtonDisabledTime, level])

  const startResendOtpTimer = () => {
    if (resendOtpTimerInterval) {
      clearInterval(resendOtpTimerInterval)
    }
    resendOtpTimerInterval = setInterval(() => {
      if (resendButtonDisabledTime <= 0) {
        clearInterval(resendOtpTimerInterval)
      } else {
        setResendButtonDisabledTime(resendButtonDisabledTime - 1)
      }
    }, 1000)
  }

  const textInputChange = (val) => {
    if (validateMobile(val)) {
      setData({
        ...data,
        mobile: val,
        check_textInputChange: true,
        isValidMobile: true
      })
    } else {
      setData({
        ...data,
        mobile: val,
        check_textInputChange: false,
        isValidMobile: false
      })
    }
  }

  const handlePasswordChange = (val) => {
    if (val.trim().length >= 4) {
      setData({
        ...data,
        password: val,
        isValidPassword: true
      })
    } else {
      setData({
        ...data,
        password: val,
        isValidPassword: false
      })
    }
  }

  const updateSecureTextEntry = () => {
    setData({
      ...data,
      secureTextEntry: !data.secureTextEntry
    })
  }

  const requestOtp = () => {
    if (level === 1) {
      if (otp.length !== 4 && !data.isValidPassword) {
        Toast.show({
          text: 'Both fields are mandatory',
          type: 'danger'
        })
        return
      }
      actions.forgetPassword({
        code: otp,
        number: data.mobile,
        new_pass: data.password
      })
    } else {
      if (!validateMobile(data.mobile)) {
        setData({
          ...data,
          isValidMobile: false
        })
        return
      }
      actions.sendOtp({
        mobile: data.mobile
      })
      setLevel(1)
    }
  }

  return (
    <View style={styles.container}>
      {/* <StatusBar backgroundColor={colors.baseColor} barStyle='light-content' /> */}
      <View style={styles.header}>
        <Text style={styles.text_header}>Forget Password</Text>
      </View>
      <Animatable.View animation='fadeInUpBig' style={styles.footer}>
        {level === 0 ? (
          <>
            <Text style={styles.text_footer}>Mobile Number</Text>
            <View style={styles.action}>
              <FontAwesome name='user-o' color={colors.darkBlue} size={20} />
              <TextInput
                placeHolder='Your Mobile'
                autoCapitalize='none'
                style={styles.textInput}
                onChangeText={textInputChange}
                value={data.mobile}
              />
              {data.check_textInputChange ? (
                <Animatable.View animation='bounceIn'>
                  <Feather name='check-circle' color='green' size={20} />
                </Animatable.View>
              ) : null}
            </View>
            {data.isValidMobile ? null : (
              <Animatable.View animation='fadeInLeft' duration={500}>
                <Text style={styles.errorMsg}>Enter valid mobile number</Text>
              </Animatable.View>
            )}
          </>
        ) : null}
        {level === 1 ? (
          <>
            <Text>Enter OTP sent to your {data.mobile}</Text>
            <View
              style={[
                styles.action,
                {
                  justifyContent: 'center',
                  alignItems: 'center'
                }
              ]}
            >
              <OTPTextInput
                handleTextChange={setOtp}
                inputCount={INPUT_COUNT}
                ref={otpInput}
              />
            </View>
            <Text style={styles.text_footer}>Password</Text>
            <View style={styles.action}>
              <Feather name='lock' color={colors.darkBlue} size={20} />
              <TextInput
                placeHolder='Your Password'
                autoCapitalize='none'
                secureTextEntry={data.secureTextEntry}
                style={styles.textInput}
                onChangeText={handlePasswordChange}
                value={data.password}
              />
              <TouchableOpacity onPress={updateSecureTextEntry}>
                {data.secureTextEntry ? (
                  <Feather name='eye-off' color='grey' size={20} />
                ) : (
                  <Feather name='eye' color='grey' size={20} />
                )}
              </TouchableOpacity>
            </View>
            {data.isValidPassword ? null : (
              <Animatable.View animation='fadeInLeft' duration={500}>
                <Text style={styles.errorMsg}>
                  Password must be atleast 4 characters
                </Text>
              </Animatable.View>
            )}

            <View
              style={[
                styles.action,
                {
                  justifyContent: 'center',
                  alignItems: 'center'
                }
              ]}
            >
              {resendButtonDisabledTime ? (
                <Text>
                  Didn&apos;t receive code? Request again in{' '}
                  {resendButtonDisabledTime}s
                </Text>
              ) : (
                <>
                  <Text>Didn&apos;t receive code? </Text>
                  <TouchableOpacity onPress={handleResendOtp}>
                    <Text
                      style={{
                        color: colors.baseColor,
                        textDecorationLine: 'underline'
                      }}
                    >
                      Request again
                    </Text>
                  </TouchableOpacity>
                </>
              )}
            </View>
          </>
        ) : null}

        <View style={styles.button}>
          <TouchableOpacity style={styles.signIn} onPress={requestOtp}>
            <View
              style={[
                styles.signIn,
                {
                  backgroundColor: colors.baseColor
                }
              ]}
            >
              <Text
                style={[
                  styles.textSign,
                  {
                    color: '#fff'
                  }
                ]}
              >
                {btnText[level]}
              </Text>
            </View>
          </TouchableOpacity>
        </View>
      </Animatable.View>
    </View>
  )
}

export default ForgetPassword

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.baseColor
  },
  header: {
    flex: 1,
    justifyContent: 'flex-end',
    paddingHorizontal: 20,
    paddingBottom: 50
  },
  footer: {
    flex: 3,
    backgroundColor: '#fff',
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    paddingHorizontal: 20,
    paddingVertical: 30
  },
  text_header: {
    color: '#fff',
    fontWeight: 'bold',
    fontSize: 30
  },
  text_footer: {
    color: colors.darkBlue,
    fontSize: 18
  },
  action: {
    flexDirection: 'row',
    marginTop: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#f2f2f2',
    paddingBottom: 5
  },
  actionError: {
    flexDirection: 'row',
    marginTop: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#FF0000',
    paddingBottom: 5
  },
  textInput: {
    flex: 1,
    marginTop: Platform.OS === 'ios' ? 0 : -12,
    paddingLeft: 10,
    color: colors.darkBlue,
    fontSize: 16
  },
  errorMsg: {
    color: '#FF0000',
    fontSize: 14
  },
  button: {
    alignItems: 'center',
    marginTop: 10
  },
  signIn: {
    width: '100%',
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10
  },
  textSign: {
    fontSize: 18,
    fontWeight: 'bold'
  }
})
