import React, { useState, useContext } from 'react'
import {
  Text,
  View,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  Platform,
  StatusBar,
  Alert
} from 'react-native'
import * as Animatable from 'react-native-animatable'
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import Feather from 'react-native-vector-icons/Feather'

import * as routes from '../../utility/routes'
import { ScrollView } from 'react-native-gesture-handler'
import { AuthContext } from '../../context/Contexts'
import colors from '../../styles/colors'

function validateEmail(email) {
  // eslint-disable-next-line no-useless-escape
  const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
  return re.test(String(email).toLowerCase())
}

function isEmpty(val) {
  return val === ''
}

const SignIn = ({ navigation }) => {
  const [data, setData] = useState({
    username: '',
    email: '',
    password: '',
    confirm_password: '',
    mobile: '',
    check_usernameInputChange: false,
    check_emailInputChange: false,
    secureTextEntry: true,
    confirmSecureTextEntry: true,
    isValidUser: true,
    isValidEmail: true,
    isValidMobile: true,
    isValidPassword: true,
    isValidConfirmPassword: true
  })

  const { actions } = useContext(AuthContext)

  const validateField = () => {
    const { username, email, password, confirm_password, mobile } = data
    if (
      isEmpty(username) ||
      isEmpty(email) ||
      isEmpty(password) ||
      isEmpty(confirm_password) ||
      isEmpty(mobile)
    ) {
      Alert.alert('Validation failed', 'All fields are mandatory')
      return false
    }
    return true
  }

  const register = () => {
    if (!validateField()) return
    validateField()
    if (
      data.isValidUser &&
      data.isValidEmail &&
      data.isValidMobile &&
      data.isValidPassword &&
      data.isValidConfirmPassword
    ) {
      actions.sendOtp({ mobile: data.mobile, user_exists: true })
      navigation.navigate(routes.SignUpOTP, {
        mobile_number: data.mobile,
        user_name: data.username,
        mail: data.email,
        pass: data.password
      })
    }
  }

  const usernameInputChange = (val) => {
    if (val.trim().length >= 4) {
      setData({
        ...data,
        username: val,
        check_usernameInputChange: true,
        isValidUser: true
      })
    } else {
      setData({
        ...data,
        username: val,
        check_usernameInputChange: false,
        isValidUser: false
      })
    }
  }

  const emailInputChange = (val) => {
    if (validateEmail(val)) {
      setData({
        ...data,
        email: val,
        check_emailInputChange: true,
        isValidEmail: true
      })
    } else {
      setData({
        ...data,
        email: val,
        check_emailInputChange: false,
        isValidEmail: false
      })
    }
  }

  const mobileInputChange = (val) => {
    const re = /^[6-9]\d{9}$/
    if (re.test(val)) {
      setData({
        ...data,
        mobile: val,
        check_mobileInputChange: true,
        isValidMobile: true
      })
    } else {
      setData({
        ...data,
        mobile: val,
        check_mobileInputChange: false,
        isValidMobile: false
      })
    }
  }

  const handlePasswordChange = (val) => {
    if (val.trim().length >= 6) {
      setData({
        ...data,
        password: val,
        isValidPassword: true
      })
    } else {
      setData({
        ...data,
        password: val,
        isValidPassword: false
      })
    }
  }

  const handleConfirmPasswordChange = (val) => {
    setData({
      ...data,
      confirm_password: val.trim(),
      isValidConfirmPassword: data.password === val && val.trim().length >= 6
    })
  }

  const updateSecureTextEntry = () => {
    setData({ ...data, secureTextEntry: !data.secureTextEntry })
  }

  const updateConfirmSecureTextEntry = () => {
    setData({ ...data, confirmSecureTextEntry: !data.confirmSecureTextEntry })
  }

  return (
    <View style={styles.container}>
      {/* <StatusBar backgroundColor={colors.baseColor} barStyle='light-content' /> */}
      <View style={styles.header}>
        <Text style={styles.text_header}>Register Now!</Text>
      </View>
      <Animatable.View animation='fadeInUpBig' style={styles.footer}>
        <ScrollView showsVerticalScrollIndicator={false}>
          <Text style={styles.text_footer}>Username</Text>
          <View style={styles.action}>
            <FontAwesome name='user-o' color='#05375a' size={20} />
            <TextInput
              placeHolder='Your Username'
              autoCapitalize='none'
              style={styles.textInput}
              onChangeText={usernameInputChange}
              value={data.username}
            />
            {data.check_usernameInputChange ? (
              <Animatable.View animation='bounceIn'>
                <Feather name='check-circle' color='green' size={20} />
              </Animatable.View>
            ) : null}
          </View>
          {data.isValidUser ? null : (
            <Animatable.View animation='fadeInLeft' duration={500}>
              <Text style={styles.errorMsg}>Username must be 4 characters</Text>
            </Animatable.View>
          )}
          <Text style={styles.text_footer}>Email</Text>
          <View style={styles.action}>
            <FontAwesome name='user-o' color='#05375a' size={20} />
            <TextInput
              placeHolder='Your email'
              autoCapitalize='none'
              style={styles.textInput}
              onChangeText={emailInputChange}
              value={data.email}
            />
            {data.check_emailInputChange ? (
              <Animatable.View animation='bounceIn'>
                <Feather name='check-circle' color='green' size={20} />
              </Animatable.View>
            ) : null}
          </View>
          {data.isValidEmail ? null : (
            <Animatable.View animation='fadeInLeft' duration={500}>
              <Text style={styles.errorMsg}>enter a valid email</Text>
            </Animatable.View>
          )}
          <Text style={styles.text_footer}>Mobile</Text>
          <View style={styles.action}>
            <FontAwesome name='user-o' color='#05375a' size={20} />
            <TextInput
              placeHolder='mobile number'
              autoCapitalize='none'
              style={styles.textInput}
              onChangeText={mobileInputChange}
              value={data.mobile}
            />
            {data.check_mobileInputChange ? (
              <Animatable.View animation='bounceIn'>
                <Feather name='check-circle' color='green' size={20} />
              </Animatable.View>
            ) : null}
          </View>
          {data.isValidMobile ? null : (
            <Animatable.View animation='fadeInLeft' duration={500}>
              <Text style={styles.errorMsg}>enter a valid mobile number</Text>
            </Animatable.View>
          )}
          <Text style={styles.text_footer}>Password</Text>
          <View style={styles.action}>
            <Feather name='lock' color='#05375a' size={20} />
            <TextInput
              placeHolder='Your Password'
              autoCapitalize='none'
              secureTextEntry={data.secureTextEntry}
              style={styles.textInput}
              onChangeText={handlePasswordChange}
              value={data.password}
            />
            <TouchableOpacity onPress={updateSecureTextEntry}>
              {data.secureTextEntry ? (
                <Feather name='eye-off' color='grey' size={20} />
              ) : (
                <Feather name='eye' color='grey' size={20} />
              )}
            </TouchableOpacity>
          </View>
          {data.isValidPassword ? null : (
            <Animatable.View animation='fadeInLeft' duration={500}>
              <Text style={styles.errorMsg}>
                Password must be atleast 6 characters
              </Text>
            </Animatable.View>
          )}
          <Text style={styles.text_footer}>Confirm Password</Text>
          <View style={styles.action}>
            <Feather name='lock' color='#05375a' size={20} />
            <TextInput
              placeHolder='Confirm Password'
              autoCapitalize='none'
              secureTextEntry={data.confirmSecureTextEntry}
              style={styles.textInput}
              onChangeText={handleConfirmPasswordChange}
              value={data.confirm_password}
            />
            <TouchableOpacity onPress={updateConfirmSecureTextEntry}>
              {data.confirmSecureTextEntry ? (
                <Feather name='eye-off' color='grey' size={20} />
              ) : (
                <Feather name='eye' color='grey' size={20} />
              )}
            </TouchableOpacity>
          </View>
          {data.isValidConfirmPassword ? null : (
            <Animatable.View animation='fadeInLeft' duration={500}>
              <Text style={styles.errorMsg}>Password must match</Text>
            </Animatable.View>
          )}
          <View style={styles.button}>
            <TouchableOpacity style={styles.signIn} onPress={register}>
              <View
                style={[styles.signIn, { backgroundColor: colors.baseColor }]}
              >
                <Text
                  style={[
                    styles.textSign,
                    {
                      color: '#fff'
                    }
                  ]}
                >
                  Sign Up
                </Text>
              </View>
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => navigation.navigate(routes.SignIn)}
              style={[
                styles.signIn,
                {
                  borderColor: colors.baseColor,
                  borderWidth: 1,
                  marginTop: 15
                }
              ]}
            >
              <Text
                style={[
                  styles.textSign,
                  {
                    color: colors.baseColor
                  }
                ]}
              >
                Sign In
              </Text>
            </TouchableOpacity>
            {/* <Text>{'\n'}</Text> */}
          </View>
        </ScrollView>
      </Animatable.View>
    </View>
  )
}

export default SignIn

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.baseColor
  },
  header: {
    flex: 1,
    justifyContent: 'flex-end',
    paddingHorizontal: 20,
    paddingBottom: 50
  },
  footer: {
    flex: 3,
    backgroundColor: '#fff',
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    paddingHorizontal: 20,
    paddingTop: 30,
    paddingBottom: 16
  },
  text_header: {
    color: '#fff',
    fontWeight: 'bold',
    fontSize: 30
  },
  text_footer: {
    color: '#05375a',
    fontSize: 18
  },
  action: {
    flexDirection: 'row',
    marginTop: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#f2f2f2',
    paddingBottom: 5
  },
  actionError: {
    flexDirection: 'row',
    marginTop: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#FF0000',
    paddingBottom: 5
  },
  textInput: {
    flex: 1,
    marginTop: Platform.OS === 'ios' ? 0 : -12,
    paddingLeft: 10,
    color: '#05375a',
    fontSize: 16
  },
  errorMsg: {
    color: '#FF0000',
    fontSize: 14
  },
  button: {
    alignItems: 'center',
    marginTop: 10
  },
  signIn: {
    width: '100%',
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10
  },
  textSign: {
    fontSize: 18,
    fontWeight: 'bold'
  }
})
