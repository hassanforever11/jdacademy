import React from 'react'
import { Text, View } from 'react-native'
import styles from '../../styles'

const Notification = () => {
  return (
    <View style={styles.container}>
      <Text>Notification Screen</Text>
    </View>
  )
}

export default Notification
