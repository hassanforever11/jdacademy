import React, { useEffect, useContext, useState } from 'react'
import {
  Container,
  Content,
  Tab,
  Header,
  Button,
  Left,
  Body,
  Icon,
  Title,
  Tabs,
  TabHeading,
  Text,
  ScrollableTab
} from 'native-base'
import { UserContext } from '../../context/Contexts'
import CourseList from './CourseList'
import * as routes from '../../utility/routes'
import colors from '../../styles/colors'
import { StyleSheet, Dimensions } from 'react-native'
import ListComponent from '../../components/ListComponent'
const tabs = ['Notes', 'Quizzes', 'Videos']

const CourseMaterial = ({ route, navigation }) => {
  const [tabData, setTabData] = useState({})
  const { userActions, courseList, videoList, notes, quiz } = useContext(
    UserContext
  )

  useEffect(() => {
    if (!notes) userActions.getNotes(route.params.sid)
    setTabData({ 0: notes, 1: quiz, 2: videoList })
  }, [notes, videoList, quiz])

  const handleTabChange = (data) => {
    switch (data.i) {
      case 0:
        if (tabData[0]) return
        tryAgainHandle(0)
        break
      case 1:
        if (tabData[1]) return
        tryAgainHandle(1)
        break
      case 2:
        if (tabData[2]) return
        tryAgainHandle(2)
        break
      default:
        break
    }
  }

  const notesHandleClick = (data) => {
    navigation.navigate(routes.Notes, { data })
  }

  const quizHandleClick = (data) => {
    navigation.navigate(routes.Quiz, { data })
  }

  const videoHandleClick = (data) => {
    navigation.navigate(routes.VideoPlayer, { data })
  }

  const tryAgainHandle = (index) => {
    switch (index) {
      case 0:
        userActions.getNotes(route.params.sid)
        break
      case 1:
        userActions.getQuiz(route.params.sid)
        break
      case 2:
        userActions.getVideos(route.params.sid)
        break
      default:
        break
    }
  }

  return (
    <Container>
      <Content scrollEnabled={false}>
        <Tabs
          tabBarUnderlineStyle={styles.tabBarUnderlineStyle}
          // tabBarActiveTextColor={}
          onChangeTab={handleTabChange}
          renderTabBar={() => (
            <ScrollableTab tabsContainerStyle={styles.tabsContainerStyle} />
          )}
        >
          <Tab
            activeTabStyle={styles.activeTabStyle}
            activeTextStyle={styles.activeTextStyle}
            tabStyle={styles.tabStyle}
            textStyle={styles.textStyle}
            heading='Notes'
          >
            <ListComponent
              titleKey='title'
              handleClick={notesHandleClick}
              data={tabData[0]}
              tryAgain={() => tryAgainHandle(0)}
            />
          </Tab>
          <Tab
            activeTabStyle={styles.activeTabStyle}
            activeTextStyle={styles.activeTextStyle}
            tabStyle={styles.tabStyle}
            textStyle={styles.textStyle}
            heading='Quizes'
          >
            <ListComponent
              titleKey='quiz_title'
              handleClick={quizHandleClick}
              data={tabData[1]}
              tryAgain={() => tryAgainHandle(1)}
              rightComponent={
                <Text style={{ color: colors.brown01 }}>Practice</Text>
              }
            />
          </Tab>
          <Tab
            activeTabStyle={styles.activeTabStyle}
            activeTextStyle={styles.activeTextStyle}
            tabStyle={styles.tabStyle}
            textStyle={styles.textStyle}
            heading='Videos'
          >
            <ListComponent
              titleKey='video_title'
              handleClick={videoHandleClick}
              data={tabData[2]}
              rightComponent={
                <Text style={{ color: colors.brown01 }}>Play Video</Text>
              }
              tryAgain={() => tryAgainHandle(2)}
            />
          </Tab>
        </Tabs>
      </Content>
    </Container>
  )
}

export default CourseMaterial

const deviceWidth = Dimensions.get('window').width

const styles = StyleSheet.create({
  tabBarUnderlineStyle: { backgroundColor: colors.baseColor },
  tabsContainerStyle: {
    backgroundColor: colors.white,
    justifyContent: 'flex-start'
  },
  activeTabStyle: {
    backgroundColor: colors.white,
    minWidth: deviceWidth / 3
  },
  activeTextStyle: {
    color: colors.baseColor,
    fontWeight: 'bold'
  },
  tabStyle: { backgroundColor: colors.white, minWidth: deviceWidth / 3 },
  textStyle: { color: colors.lightBlack }
})
