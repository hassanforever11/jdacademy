import React, { useContext } from 'react'
import { StyleSheet, View, Image } from 'react-native'
import {
  Container,
  Content,
  Header,
  Button,
  Left,
  Body,
  Text,
  Icon,
  Title,
  Footer,
  FooterTab,
  H3
} from 'native-base'
import colors from '../../styles/colors'
import * as routes from '../../utility/routes'
import { Paragraph } from 'react-native-paper'
import { UserContext, AuthContext } from '../../context/Contexts'

export default function CourseDetail({ route, navigation }) {
  const { params } = route
  const { userActions } = useContext(UserContext)
  const { uid } = useContext(AuthContext)

  const {
    course_image,
    course_description,
    course_title,
    course_price,
    course_offer_price,
    is_free,
    cid
  } = params.details || {}

  const goToSessionList = () => {
    navigation.navigate(routes.SessionList, { cid })
  }

  const addToLibrary = () => {
    userActions.addCourse({
      uid,
      is_remove: '0',
      field_purchased_courses: {
        und: [cid]
      }
    })
  }

  return (
    <Container>
      <Content style={{ padding: 20 }}>
        <View style={styles.priceContainer}>
          <Image
            style={styles.courseImg}
            resizeMethod='resize'
            source={{
              uri: course_image
            }}
          />
          <View style={{ marginHorizontal: 20 }}>
            {course_title ? (
              <H3 style={[styles.boldText]}>
                {course_title || 'Course Details'}
              </H3>
            ) : null}
            <Text style={styles.descriptionTitle}>Online Course</Text>
            <View style={styles.priceContainer}>
              {is_free === '1' ? (
                <Text style={styles.price}>Free</Text>
              ) : course_offer_price !== '0' ? (
                <>
                  <Text style={styles.price}>
                    &#x20B9;{course_offer_price}&nbsp;
                  </Text>
                  <Text style={styles.strikeThrough}>
                    &#x20B9;{course_price}
                  </Text>
                </>
              ) : (
                <Text style={styles.price}>&#x20B9;{course_price}&nbsp;</Text>
              )}
            </View>
          </View>
        </View>
        {course_description ? (
          <View style={{ textAlign: 'left', marginTop: 20 }}>
            <Paragraph>
              <Text style={styles.descriptionTitle}>
                Course Description:
                {'\n'}
              </Text>
              {course_description}
            </Paragraph>
          </View>
        ) : null}
      </Content>
      <Footer>
        <FooterTab>
          <Button onPress={goToSessionList} style={styles.footerBtn}>
            <Text style={styles.footerText}>OPEN</Text>
          </Button>
        </FooterTab>
        <FooterTab>
          <Button onPress={addToLibrary} style={styles.addLibBtn}>
            <Text style={[styles.footerText, { color: colors.white }]}>
              Add To Library
            </Text>
          </Button>
        </FooterTab>
      </Footer>
    </Container>
  )
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row'
  },
  courseImg: {
    width: 150,
    height: 220
  },
  name: {
    fontSize: 28,
    color: colors.lightBlack,
    fontWeight: 'bold'
  },
  price: {
    fontSize: 16,
    fontWeight: 'bold',
    color: colors.baseColor
  },
  descriptionTitle: {
    fontSize: 17,
    fontWeight: 'bold',
    color: colors.lightBlack
  },
  priceContainer: {
    flexDirection: 'row'
  },
  strikeThrough: {
    fontSize: 12,
    textDecorationLine: 'line-through',
    textAlignVertical: 'bottom'
  },
  footerText: {
    lineHeight: 20,
    fontSize: 16,
    fontWeight: 'bold',
    color: colors.baseColor
  },
  footerBtn: {
    backgroundColor: colors.white,
    borderTopWidth: 0.5,
    borderTopColor: colors.baseColor
  },
  addLibBtn: {
    backgroundColor: colors.baseColor
  },
  boldText: {
    fontWeight: 'bold'
  }
})
