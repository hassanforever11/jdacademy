import React, { useContext, useEffect } from 'react'
import { Text, View } from 'react-native'
import styles from '../../styles'
import { AuthContext, UserContext } from '../../context/Contexts'
import CourseList from '../Shop/CourseList'

const MyLibrary = () => {
  const { uid } = useContext(AuthContext)
  const { userActions, library, userLoading } = useContext(UserContext)

  useEffect(() => {
    if (uid) userActions.getMyLibrary(uid)
  }, [uid])

  const tryAgainHandler = () => {
    userActions.getMyLibrary(uid)
  }

  return (
    <CourseList
      data={library}
      userLoading={userLoading}
      tryAgain={tryAgainHandler}
    />
  )
}

export default MyLibrary
