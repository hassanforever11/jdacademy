import React, { Component, useEffect, useState } from 'react'
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Button,
  Left,
  Right,
  Body,
  Icon,
  Text
} from 'native-base'
import QuizContent from './QuizContent'
import { goBack } from '../utility/NavigationService'
import { StyleSheet, View, ScrollView } from 'react-native'
import colors from '../styles/colors'
import * as Animatable from 'react-native-animatable'

function noRef() {}

const QuizSubmitPage = ({ selectedData, quizData }) => {
  const [score, setScore] = useState(0)

  useEffect(() => {
    let tempScore = 0
    Object.keys(selectedData).map((key) => {
      if (selectedData[key].correct === '1') tempScore++
    })
    setScore(tempScore)
  }, [selectedData])

  return (
    <Container style={styles.container}>
      <View style={styles.header}>
        <View
          style={{
            borderWidth: 8,
            borderColor: colors.white,
            borderRadius: 60,
            justifyContent: 'center',
            alignItems: 'center',
            paddingBottom: 8
          }}
        >
          <Icon
            name='thumbs-up'
            style={{
              fontSize: 60,
              color: colors.white,
              paddingHorizontal: 22,
              paddingVertical: 16
            }}
          />
        </View>
        <Title>{`Your Score: ${score} out of ${quizData.length}`}</Title>
      </View>
      <Animatable.View animation='fadeInUpBig' style={styles.footer}>
        <Title style={{ color: colors.lightBlack }}>Correct Answers</Title>
        <ScrollView showsVerticalScrollIndicator={false}>
          <QuizContent
            data={quizData}
            selectedData={selectedData}
            resultPage
            flagData={noRef}
            handleClick={noRef}
            handleFlag={noRef}
          />
        </ScrollView>
      </Animatable.View>
      <Footer>
        <FooterTab>
          <Button
            style={{ backgroundColor: colors.baseColor }}
            full
            onPress={goBack}
          >
            <Title>Done</Title>
          </Button>
        </FooterTab>
      </Footer>
    </Container>
  )
}

export default QuizSubmitPage

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.baseColor
  },
  header: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 20,
    paddingVertical: 20
  },
  footer: {
    flex: 3,
    backgroundColor: '#fff',
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    paddingHorizontal: 20,
    paddingVertical: 10
  }
})
