import React from 'react'
import { View, Image, StyleSheet } from 'react-native'
import { Button, Text } from 'native-base'
import colors from '../styles/colors'

export default function NoDataFound({ tryAgain }) {
  return (
    <View style={styles.noData}>
      <Image source={require('../../assets/images/noData.png')} />
      {tryAgain ? (
        <Button
          style={{ backgroundColor: colors.baseColor }}
          rounded
          onPress={tryAgain}
        >
          <Text
            style={{
              fontWeight: 'bold',
              color: colors.white,
              // textDecorationLine: 'underline',
              paddingHorizontal: 8,
              fontSize: 18
            }}
          >
            Try Again
          </Text>
        </Button>
      ) : null}
    </View>
  )
}

const styles = StyleSheet.create({
  noData: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  }
})
