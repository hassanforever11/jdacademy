import React, { useContext, useState, useEffect } from 'react'
import * as Animatable from 'react-native-animatable'
import { View } from 'react-native'
import {
  Container,
  Header,
  Content,
  Card,
  CardItem,
  Text,
  Body,
  Left,
  Button,
  Title,
  Right,
  Footer,
  FooterTab,
  Toast
} from 'native-base'

import colors from '../styles/colors'
import { StyleSheet } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'
import { AuthContext, UserContext } from '../context/Contexts'
import * as routes from '../utility/routes'
import { showToast } from '../utility/commonMethods'
import Ionicons from 'react-native-vector-icons/Ionicons'

const ExamDetailCard = ({
  handleClick,
  topic,
  uid,
  actions,
  is_remove,
  index
}) => {
  return (
    <Animatable.View
      animation='fadeInUp'
      easing='ease-in-out'
      duration={500 + index * 200}
    >
      <TouchableOpacity
        activeOpacity={0.5}
        onPress={() => {
          handleClick(
            {
              uid,
              is_remove: is_remove ? '1' : '0',
              field_course_category: {
                und: [topic.tid]
              }
            },
            actions
          )
        }}
      >
        <Card>
          <CardItem
            header
            style={{
              backgroundColor: is_remove ? colors.baseColor : colors.white
            }}
          >
            <Text
              style={{
                color: is_remove ? colors.white : colors.black,
                fontWeight: 'bold'
              }}
            >
              {topic.name}
            </Text>
            <Right style={{ position: 'absolute', right: 20 }}>
              {is_remove ? (
                <Ionicons
                  name={'checkmark-circle-sharp'}
                  size={24}
                  style={{ color: is_remove ? colors.white : colors.black }}
                />
              ) : null}
            </Right>
          </CardItem>
        </Card>
      </TouchableOpacity>
    </Animatable.View>
  )
}

const ExamDetails = ({ route, navigation }) => {
  const { data = [], title } = route.params
  const { actions, uid, exam_category } = useContext(AuthContext)
  const { userActions } = useContext(UserContext)

  const [examCategoryHash, setExamCategoryHash] = useState({})

  useEffect(() => {
    if (exam_category && exam_category.length) {
      const obj = {}
      exam_category.map((course) => (obj[course.name] = true))
      setExamCategoryHash(obj)
    }
  }, [exam_category])

  const goBack = () => {
    navigation.goBack()
  }

  const goToHomePage = () => {
    if (!exam_category || !exam_category.length) {
      Toast.show({
        text: 'Atleast one Exam should be selected',
        buttonText: 'Okay',
        type: 'danger'
      })
    } else {
      navigation.navigate(routes.Home)
    }
  }

  const handleAddCourse = (data, action) => {
    if (data.is_remove === '1' && exam_category.length === 1) {
      showToast('Atleast one category should be selected')
    } else {
      userActions.addCourse(data, action)
    }
  }

  return (
    <Container>
      <Header style={{ backgroundColor: colors.baseColor }}>
        <Left>
          <Button transparent onPress={goBack}>
            <Ionicons
              name='arrow-back'
              style={{ color: colors.white }}
              size={24}
            />
          </Button>
        </Left>
        <Body>
          <Title style={styles.title}>{title}</Title>
        </Body>
        <Right></Right>
      </Header>
      <Content padder>
        {data.map((topic, i) => {
          if (examCategoryHash[topic.name]) {
            return (
              <ExamDetailCard
                key={i}
                index={i}
                is_remove={1}
                handleClick={handleAddCourse}
                topic={topic}
                uid={uid}
                actions={actions}
              />
            )
          } else {
            return (
              <ExamDetailCard
                key={i}
                index={i}
                handleClick={handleAddCourse}
                topic={topic}
                uid={uid}
                actions={actions}
                is_remove={0}
              />
            )
          }
        })}
      </Content>
      <Footer>
        <FooterTab>
          <Button onPress={goToHomePage} full style={styles.footerBtn}>
            <Text style={styles.footerText}>Done</Text>
          </Button>
        </FooterTab>
      </Footer>
    </Container>
  )
}

export default ExamDetails

const styles = StyleSheet.create({
  title: {
    fontWeight: 'bold',
    fontSize: 18
  },
  footerText: {
    fontSize: 20,
    lineHeight: 20,
    fontWeight: 'bold',
    color: colors.baseColor
  },
  footerBtn: {
    backgroundColor: colors.white,
    borderTopWidth: 0.5,
    borderTopColor: colors.gray02
  }
})
