import React, { useEffect, useState, useContext } from 'react'
import {
  Container,
  Header,
  Title,
  Content,
  Button,
  Icon,
  Left,
  Body,
  Text,
  Right
} from 'native-base'
import { StyleSheet, Alert } from 'react-native'
import colors from '../styles/colors'
import { getTime } from '../utility/commonMethods'
import QuizContent from './QuizContent'
import QuizSubmitPage from './QuizSubmitPage'
import { UserContext } from '../context/Contexts'

let quizTimer
const Quiz = ({ route }) => {
  const { quiz_title, qid } = route.params.data

  const [time, setTime] = useState(0)
  const [play, setPlay] = useState(false)
  const [submit, setSubmit] = useState(false)
  const [flagged, setflagged] = useState(0)
  const [answered, setanswered] = useState(0)
  const [selectedData, setSelectedData] = useState({})
  const [flagData, setFlagData] = useState({})
  const { userActions, quizData } = useContext(UserContext)

  const handleOptionClick = (count, ans, correct, id) => {
    if (!selectedData[count]) setanswered(answered + 1)
    setSelectedData({
      ...selectedData,
      [count]: { ans, id, correct }
    })
  }

  const handleFlag = (count) => {
    //If already flagged reduce else increase
    flagData[count] ? setflagged(flagged - 1) : setflagged(flagged + 1)
    setFlagData({
      ...flagData,
      [count]: flagData[count] ? false : true
    })
  }

  useEffect(() => {
    userActions.getQuizData(qid)
  }, [])

  useEffect(() => {
    if (play) startTimer()
    return () => {
      clearInterval(quizTimer)
    }
  }, [time, play])

  const startTimer = () => {
    if (quizTimer) {
      clearInterval(quizTimer)
    }
    quizTimer = setInterval(() => {
      setTime(time + 1)
    }, 1000)
  }

  const handleSubmit = () => {
    Alert.alert(
      'Submit Test?',
      `${answered} answered ,${flagged} marked for review, ${
        quizData.length - answered
      } unanswered`,
      [
        {
          text: 'Resume'
        },
        { text: 'Submit', onPress: () => setSubmit(true) }
      ]
    )
  }

  if (submit) {
    return <QuizSubmitPage quizData={quizData} selectedData={selectedData} />
  }

  return (
    <Container>
      <Header style={styles.header}>
        <Left>
          <Icon
            onPress={() => setPlay(!play)}
            name={play ? 'pause-circle' : 'play-circle'}
            style={{ color: colors.white }}
          />
        </Left>
        <Body>
          <Title>{time ? getTime(time) : quiz_title}</Title>
        </Body>
        <Right>
          <Button transparent onPress={handleSubmit}>
            <Title>Submit</Title>
          </Button>
        </Right>
      </Header>
      <Content padder>
        <QuizContent
          data={quizData}
          selectedData={selectedData}
          flagData={flagData}
          handleClick={handleOptionClick}
          handleFlag={handleFlag}
        />
      </Content>
    </Container>
  )
}

export default Quiz

const styles = StyleSheet.create({
  header: {
    backgroundColor: colors.baseColor
  },
  timer: {
    color: colors.black
  },
  boldText: {
    fontWeight: 'bold'
  }
})
