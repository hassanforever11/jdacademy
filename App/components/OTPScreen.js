import React, { useRef, useState, useEffect } from 'react'
import {
  StyleSheet,
  View,
  Alert,
  TouchableOpacity,
  Dimensions
} from 'react-native'
import OTPTextInput from 'react-native-otp-textinput'
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Button,
  Left,
  Right,
  Body,
  Icon,
  Text
} from 'native-base'
import * as Animatable from 'react-native-animatable'
import colors from '../styles/colors'
import { showToast } from '../utility/commonMethods'
import RNOtpVerify from 'react-native-otp-verify'
import Ionicons from 'react-native-vector-icons/Ionicons'

const messageRecieveImage = require('../../assets/images/messageReceive.png')
const RESEND_OTP_TIME_LIMIT = 50 // in secs
let resendOtpTimerInterval

const INPUT_COUNT = 4

const OTPScreen = ({
  verifyOtp,
  resendOtp,
  mobile,
  footerText,
  navigation
}) => {
  const otpInput = useRef(null)
  const [otp, setOtp] = useState('')

  const [resendButtonDisabledTime, setResendButtonDisabledTime] = useState(
    RESEND_OTP_TIME_LIMIT
  )

  const setText = (otp) => {
    otpInput?.current?.setValue(otp)
  }

  // handle auto read otp once we received message and auto submit
  const handleAutoReadOtp = (message) => {
    try {
      if (message) {
        var rx = /(\d{4})/
        let extractedOtp = rx.exec(message)
        if (extractedOtp && extractedOtp.length) {
          setText(extractedOtp[0].toString())
          verifyOtp(extractedOtp[0])
        }
      }
    } catch (error) {
      showToast('Unable to read message', 'danger')
    }
  }

  useEffect(() => {
    // docs: https://github.com/faizalshap/react-native-otp-verify
    RNOtpVerify.getOtp()
      .then((p) => RNOtpVerify.addListener(handleAutoReadOtp))
      .catch((error) => {
        showToast('Auto Read Otp Not working', 'danger')
      })

    // remove listener on unmount
    return () => {
      RNOtpVerify.removeListener()
    }
  }, [])

  const handleResendOtp = () => {
    setResendButtonDisabledTime(RESEND_OTP_TIME_LIMIT)
    resendOtp()
  }

  useEffect(() => {
    startResendOtpTimer()

    return () => {
      if (resendOtpTimerInterval) {
        clearInterval(resendOtpTimerInterval)
      }
    }
  }, [resendButtonDisabledTime])

  const startResendOtpTimer = () => {
    if (resendOtpTimerInterval) {
      clearInterval(resendOtpTimerInterval)
    }
    resendOtpTimerInterval = setInterval(() => {
      if (resendButtonDisabledTime <= 0) {
        clearInterval(resendOtpTimerInterval)
      } else {
        setResendButtonDisabledTime(resendButtonDisabledTime - 1)
      }
    }, 1000)
  }

  // const clearText = () => {
  //   otpInput.current.clear()
  // }

  const handleOtpVerification = () => {
    if (otp.length > INPUT_COUNT) {
      Alert.alert(`Enter ${INPUT_COUNT} digit OTP`)
      return
    }
    verifyOtp(otp)
  }

  const goBack = () => {
    navigation.goBack()
  }

  return (
    <Container>
      <Header style={{ backgroundColor: colors.baseColor }}>
        <Left>
          <Button transparent onPress={goBack}>
            <Ionicons
              name='arrow-back'
              style={{ color: colors.white }}
              size={24}
            />
          </Button>
        </Left>
        <Body>
          <Title style={{ color: colors.white }}>Verify Mobile</Title>
        </Body>
        <Right>
          {/* <Button transparent>
            <Text>Cancel</Text>
          </Button> */}
        </Right>
      </Header>
      <Content contentContainerStyle={styles.container}>
        <Animatable.Image
          animation='bounceIn'
          duraton='1000'
          source={messageRecieveImage}
          // style={styles.logo}
          resizeMode='stretch'
        />
        <Animatable.View
          animation='fadeInUpBig'
          style={{
            width: '100%',
            justifyContent: 'center',
            alignItems: 'center'
          }}
        >
          <Text>Enter OTP sent to {mobile}</Text>
          <View
            style={[
              styles.action,
              { justifyContent: 'center', alignItems: 'center' }
            ]}
          >
            <OTPTextInput
              handleTextChange={setOtp}
              inputCount={INPUT_COUNT}
              ref={otpInput}
            />
          </View>

          <View
            style={[
              styles.action,
              { justifyContent: 'center', alignItems: 'center' }
            ]}
          >
            {resendButtonDisabledTime ? (
              <Text>
                Didn&apos;t receive code? Request again in{' '}
                {resendButtonDisabledTime}s
              </Text>
            ) : (
              <>
                <Text>Didn&apos;t receive code? </Text>
                <TouchableOpacity onPress={handleResendOtp}>
                  <Text
                    style={{
                      color: colors.baseColor,
                      textDecorationLine: 'underline'
                    }}
                  >
                    Request again
                  </Text>
                </TouchableOpacity>
              </>
            )}
          </View>
        </Animatable.View>
      </Content>
      <Footer>
        <FooterTab>
          <Button onPress={handleOtpVerification} full style={styles.footerBtn}>
            <Text style={styles.footerText}>{footerText}</Text>
          </Button>
        </FooterTab>
      </Footer>
    </Container>
  )
}

export default OTPScreen

const { height } = Dimensions.get('screen')
const height_logo = height * 0.45

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  action: {
    alignItems: 'center',
    marginTop: 10,
    paddingHorizontal: 10,
    flexDirection: 'row'
  },
  button: {
    width: '50%',
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
    marginHorizontal: 1
  },
  footerText: {
    fontSize: 20,
    lineHeight: 20,
    fontWeight: 'bold',
    color: colors.baseColor
  },
  logo: {
    width: height_logo,
    height: height_logo
  },
  footerBtn: {
    backgroundColor: colors.white,
    borderTopWidth: 0.5,
    borderTopColor: colors.gray02
  }
})
