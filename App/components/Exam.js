import React, { useEffect, useContext } from 'react'
import {
  Container,
  Header,
  Content,
  Card,
  CardItem,
  Text,
  Body,
  Icon,
  Left,
  Button,
  Title,
  Right,
  Toast
} from 'native-base'

import * as routes from '../utility/routes'
import colors from '../styles/colors'
import { StyleSheet } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'
import { AuthContext, UserContext } from '../context/Contexts'
import Ionicons from 'react-native-vector-icons/Ionicons'

const Exam = ({ navigation }) => {
  const { userActions, category } = useContext(UserContext)
  const { exam_category } = useContext(AuthContext)

  useEffect(() => {
    userActions.getAllCategory()
  }, [])

  const goBack = () => {
    if (exam_category && exam_category.length) {
      navigation.goBack()
    } else {
      Toast.show({
        text: 'Atleast one Exam should be selected',
        buttonText: 'Okay',
        type: 'danger'
      })
    }
  }

  return (
    <Container>
      <Header style={{ backgroundColor: colors.baseColor }}>
        <Left>
          <Button transparent onPress={goBack}>
            <Ionicons
              name='arrow-back'
              style={{ color: colors.white }}
              size={24}
            />
          </Button>
        </Left>
        <Body>
          <Title style={styles.title}>Choose Course</Title>
        </Body>
        <Right></Right>
      </Header>
      <Content padder>
        <Card transparent>
          <CardItem>
            <Text style={styles.title}>All Categories</Text>
          </CardItem>
        </Card>
        {category.map((exam, i) => {
          return (
            <TouchableOpacity
              activeOpacity={0.5}
              key={i}
              onPress={() => {
                navigation.navigate(routes.ExamDetails, {
                  title: exam.parent,
                  data: exam.child
                })
              }}
            >
              <Card>
                <CardItem header>
                  <Text>{exam.parent}</Text>
                  <Right style={{ position: 'absolute', right: 20 }}>
                    <Ionicons name='arrow-forward' size={16} />
                  </Right>
                </CardItem>
              </Card>
            </TouchableOpacity>
          )
        })}
        {/* {category.length === 0 ? <Text>No Data found</Text> : null} */}
        {/* <Card>
          <CardItem header>
            <Text>NativeBase</Text>
          </CardItem>
          <CardItem>
            <Body>
              <Text>//Your text here</Text>
            </Body>
          </CardItem>
          <CardItem footer>
            <Text>GeekyAnts</Text>
          </CardItem>
        </Card> */}
      </Content>
    </Container>
  )
}

export default Exam

const styles = StyleSheet.create({
  title: {
    fontWeight: 'bold',
    fontSize: 18
  }
})
